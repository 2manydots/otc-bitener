<?php
/* Template name: Checkout */

get_header();
?>
<main class="main">
    <div class="row">
        <div class="medium-12 page-title columns">
            <h1><?php the_title();?></h1>
        </div>
    </div>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="checkout-wrapper">
            <div class="row">
                <div class="medium-10 medium-push-1 columns shopping-cart-big">
                    <div class="cart">
                        <div class="cart-memory clearfix">
                            <ul class="merchant-list"></ul>
                            <div class="total-pro">
                                <div class="total-text">Totaal</div>
                                <div class="total-price">€<span class="total-pro-price"></span></div>
                            </div>
                        </div>
                        <a href="#go-to-form" class="btn btn_blue" id="cart-btn">BESTELLING AFRONDEN</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="medium-8 medium-offset-1 columns">
                    <div class="checkout-form clearfix" id="go-to-form">
                        <h2>BESTELLING AFRONDEN</h2>
                        <?php echo do_shortcode( '[contact-form-7 id="18460" title="BESTELLING AFRONDEN"]' );?>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; endif;?>
</main>
<?php
get_footer();
