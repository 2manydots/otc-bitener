<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

   <main>
       <?php get_template_part('parts/breadcrumbs'); ?>
       <?php if ( have_posts() ) : ?>
           <?php
           $image_or_slider = get_field('image_or_slider');
           if($image_or_slider == 'image'){?>
               <?php if($top_image = get_field('top_image')){?>
                   <?php
                   $image_size = 'image-section';
                   $image_size_2 = 'image-section-2';
                   $image_alt = $top_image['alt'];
                   $image_thumb = $top_image['sizes'][ $image_size ];
                   $image_thumb_2 = $top_image['sizes'][ $image_size_2 ];
                   $width = ($top_image['sizes'][ $image_size_2 . '-width' ])/2;
                   $height = ($top_image['sizes'][ $image_size_2 . '-height' ])/2;
                   ?>
                   <div class="top_image_wrapper">
                       <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                            srcset="<?php echo $image_thumb_2; ?> 2x,
                                <?php echo $image_thumb; ?> 1x"
                            alt="<?php echo $image_left_alt;?>">
                       <?php if(get_field('show_title')){?>
                           <div class="row top_image_title_wrapper">
                               <div class="large-12 medium-12 small-12 columns">
                                   <h1><?php the_title();?></h1>
                               </div>
                           </div>
                        <?php } ?>
                   </div>
               <?php } else { ?>
                   <?php if(get_field('show_title')){?>
                       <div class="row page-title">
                           <div class="large-12 medium-12 small-12 columns">
                               <h1><?php the_title();?></h1>
                           </div>
                       </div>
                   <?php } ?>
               <?php } ?>
           <?php } elseif($image_or_slider == 'slider') {?>
                <?php if( have_rows('top_slider') ):?>
                    <div class="top_slider_wrapper desktop">
                        <?php while ( have_rows('top_slider') ) : the_row();?>
                            <div class="slick-slide">
                                <?php if($image = get_sub_field('desktop_image')){?>
                                    <?php 
                                        $image_size = 'image-section';
                                        $image_size_2 = 'image-section-2';
                                        $image_alt = $image['alt'];
                                        $image_thumb = $image['sizes'][ $image_size ];
                                        $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                        $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                        $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                    ?>
                                    <div class="desktop_image image_wrapper">
                                        <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                            srcset="<?php echo $image_thumb_2; ?> 2x,
                                                    <?php echo $image_thumb; ?> 1x"
                                            alt="<?php echo $image_left_alt;?>">
                                    </div>
                                <?php } ?>
                            </div>
                        <?php endwhile;?>
                    </div>
                    <div class="top_slider_wrapper mobile">
                        <?php while ( have_rows('top_slider') ) : the_row();?>
                            <div class="slick-slide">
                                <?php if($image = get_sub_field('mobile_image')){?>
                                    <?php
                                    $image_size = 'slider-mobile';
                                    $image_size_2 = 'slider-mobile-2';
                                    $image_alt = $image['alt'];
                                    $image_thumb = $image['sizes'][ $image_size ];
                                    $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                    $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                    $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                    ?>
                                    <div class="desktop_image image_wrapper">
                                        <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                            srcset="<?php echo $image_thumb_2; ?> 2x,
                                                    <?php echo $image_thumb; ?> 1x"
                                            alt="<?php echo $image_left_alt;?>">
                                    </div>
                                <?php } ?>
                            </div>
                        <?php endwhile;?>
                    </div>
                <?php endif;?>
           <?php } else {?>
               <div class="row page-title">
                   <div class="large-12 medium-12 small-12 columns">
                       <h1><?php the_title();?></h1>
                   </div>
               </div>
           <?php }; ?>
           
           <?php $featured_image_url = get_field('featured_image');?>
           <div class="featured_image" data-width="<?php echo $featured_image_url['sizes']['product-thumbnail-small-2-width'];?>" data-height="<?php echo $featured_image_url['sizes']['product-thumbnail-small-2-height'];?>" data-small="<?php echo $featured_image_url['sizes']['product-thumbnail-small'];?>" data-big="<?php echo $featured_image_url['sizes']['product-thumbnail-small-2'];?>"></div>
           <div class="product-description wide-image-section <?php echo get_sub_field('section_background_color');?>">
               <?php $image_position = get_field('image_position');?>
               <?php if($image_position == 'img_left' ){?>
                   <div class="row">
                       <div class="large-5 medium-5 medium-offset-1 small-12 columns">
                           <div class="section-table">
                               <div class="section-cell">
                                   <?php if($image = get_field('product_image')){?>
                                       <?php
                                       $image_size = 'medium-content-image';
                                       $image_size_2 = 'medium-content-image-2';
                                       $image_alt = $image['alt'];
                                       $image_thumb = $image['sizes'][ $image_size ];
                                       $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                       $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                       $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                       ?>
                                       <div class="image_image image_wrapper">
                                           <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                             <?php echo $image_thumb; ?> 1x"
                                                alt="<?php echo $image_left_alt;?>">
                                       </div>
                                   <?php } ?>
                               </div>
                           </div>
                       </div>
                       <div class="large-4 medium-4 medium-offset-1 small-12 columns">
                           <div class="section-table">
                               <div class="section-cell">
                                   <div class="section-content">
                                       <?php $product_description = get_field('product_description');?>
                                       <?php if($product_description){?>
                                           <div class="text">
                                               <?php echo $product_description;?>
                                           </div>
                                       <?php } ?>
                                       <div class="add-product">
                                           <div class="quantity-wrapper">
                                               <span>Aantal</span>
                                               <input type="text" class="quantity">
                                           </div>
                                           <span class="btn btn_blue btn_add" data-price="<?php echo get_field('price_field');?>" data-title="<?php echo get_field('title_for_display_black'); ?>">Bestellen</span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               <?php } elseif ($image_position == 'img_right'){ ?>
                   <div class="row">
                       <div class="large-5 medium-5 medium-push-6 small-12 columns">
                           <div class="section-table">
                               <div class="section-cell">
                                   <?php if($image = get_field('product_image')){?>
                                       <?php
                                       $image_size = 'medium-content-image';
                                       $image_size_2 = 'medium-content-image-2';
                                       $image_alt = $image['alt'];
                                       $image_thumb = $image['sizes'][ $image_size ];
                                       $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                       $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                       $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                       ?>
                                       <div class="image_image image_wrapper">
                                           <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                             <?php echo $image_thumb; ?> 1x"
                                                alt="<?php echo $image_left_alt;?>">
                                       </div>
                                   <?php } ?>
                               </div>
                           </div>
                       </div>
                       <div class="large-4 medium-4 medium-pull-4 small-12 columns">
                           <div class="section-table">
                               <div class="section-cell">
                                   <div class="section-content">
                                       <?php $product_description = get_field('product_description');?>
                                       <?php if($product_description){?>
                                           <div class="text">
                                               <?php echo $product_description;?>
                                           </div>
                                       <?php } ?>
                                       <div class="add-product">
                                           <div class="quantity-wrapper">
                                               <span>Aantal</span>
                                               <input type="text" class="quantity">
                                           </div>
                                           <span class="btn btn_blue btn_add" data-price="<?php echo get_field('price_field');?>" data-title="<?php echo get_field('title_for_display_black'); ?>">Bestellen</span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               <?php }?>
           </div>







           <?php while ( have_posts() ) : the_post(); ?>
               <?php if( have_rows('flexible_sections') ):?>
                   <div class="flexible_sections_wrapper">
                       <?php while ( have_rows('flexible_sections') ) : the_row();?>
                           <?php if( get_row_layout() == 'about_section' ):?>
                               <div class="flex-section about-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="row">
                                       <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                                           <div class="section-content">
                                               <?php $text = get_sub_field('text');?>
                                               <?php if($text){?>
                                                   <div class="text">
                                                       <?php echo $text;?>
                                                       <?php
                                                       $button_title = get_sub_field('section_button_title');
                                                       $button_type_of_link = get_sub_field('section_button_type');
                                                       $button_external_link = get_sub_field('section_button_external_link');
                                                       $button_page_link = get_sub_field('section_button_page_link');
                                                       $button_file_link = get_sub_field('section_button_file_link');
                                                       ?>
                                                       <?php if($button_type_of_link == 'external' && $button_title){?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                           <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                       <?php } ?>
                                                       <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                   </div>
                                               <?php } ?>

                                           </div>
                                       </div>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'text_full_width' ):?>
                               <div class="flex-section text-full-width-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="row">
                                       <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                                           <div class="section-content">
                                               <?php $text = get_sub_field('text');?>
                                               <?php if($text){?>
                                                   <div class="text">
                                                       <?php echo $text;?>
                                                       <?php
                                                       $button_title = get_sub_field('section_button_title');
                                                       $button_type_of_link = get_sub_field('section_button_type');
                                                       $button_external_link = get_sub_field('section_button_external_link');
                                                       $button_page_link = get_sub_field('section_button_page_link');
                                                       $button_file_link = get_sub_field('section_button_file_link');
                                                       ?>
                                                       <?php if($button_type_of_link == 'external' && $button_title){?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                           <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                       <?php } ?>
                                                       <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?></div>
                                               <?php } ?>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'text_narrow' ):?>
                               <div class="flex-section text-narrow-width-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="row">
                                       <div class="large-6 medium-6 medium-offset-3 small-12 columns">
                                           <div class="section-content">
                                               <?php $text = get_sub_field('text');?>
                                               <?php if($text){?>
                                                   <div class="text">
                                                       <?php echo $text;?>
                                                       <?php
                                                       $button_title = get_sub_field('section_button_title');
                                                       $button_type_of_link = get_sub_field('section_button_type');
                                                       $button_external_link = get_sub_field('section_button_external_link');
                                                       $button_page_link = get_sub_field('section_button_page_link');
                                                       $button_file_link = get_sub_field('section_button_file_link');
                                                       ?>
                                                       <?php if($button_type_of_link == 'external' && $button_title){?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                           <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                       <?php } ?>
                                                       <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                   </div>
                                               <?php } ?>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'text_narrow_with_icons' ):?>
                               <div class="flex-section text-narrow-width-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="section-content">
                                       <div class="row">
                                           <div class="large-6 medium-6 medium-offset-3 small-12 columns">
                                               <?php $text = get_sub_field('text');?>
                                               <?php if($text){?>
                                                   <div class="text">
                                                       <?php echo $text;?>
                                                       <?php
                                                       $button_title = get_sub_field('section_button_title');
                                                       $button_type_of_link = get_sub_field('section_button_type');
                                                       $button_external_link = get_sub_field('section_button_external_link');
                                                       $button_page_link = get_sub_field('section_button_page_link');
                                                       $button_file_link = get_sub_field('section_button_file_link');
                                                       ?>
                                                       <?php if($button_type_of_link == 'external' && $button_title){?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                           <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                       <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                           <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                       <?php } ?>
                                                       <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                   </div>
                                               <?php } ?>
                                           </div>
                                       </div>
                                       <?php if( have_rows('images_sections') ):?>
                                           <div class="images_sections_wrapper">
                                               <?php while ( have_rows('images_sections') ) : the_row();?>
                                                   <?php $section_description = get_sub_field('section_description');?>
                                                   <?php if($section_description){?>
                                                       <div class="row">
                                                           <div class="large-6 medium-6 medium-offset-3 small-12 columns">
                                                               <div class="section_description"><?php echo $section_description;?></div>
                                                           </div>
                                                       </div>
                                                   <?php } ?>
                                                   <?php if( have_rows('images_list') ):?>
                                                       <div class="row">
                                                           <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                                                               <div class="images_list_wrapper">
                                                                   <?php while ( have_rows('images_list') ) : the_row();?>
                                                                       <div class="images_list_item">
                                                                           <?php if($image = get_sub_field('image')){?>
                                                                               <?php
                                                                               $image_size = 'narrow-section-image';
                                                                               $image_size_2 = 'narrow-section-image-2';
                                                                               $image_alt = $image['alt'];
                                                                               $image_thumb = $image['sizes'][ $image_size ];
                                                                               $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                                               $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                                               $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                                               ?>
                                                                               <div class="image_image image_wrapper">
                                                                                   <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                                        srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                                     <?php echo $image_thumb; ?> 1x"
                                                                                        alt="<?php echo $image_left_alt;?>">
                                                                               </div>
                                                                           <?php } ?>
                                                                           <?php $image_description = get_sub_field('image_description');?>
                                                                           <?php if($image_description){?>
                                                                               <span class="image_description"><?php echo $image_description;?></span>
                                                                           <?php } ?>
                                                                       </div>
                                                                   <?php endwhile;?>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   <?php endif;?>
                                               <?php endwhile;?>
                                           </div>
                                       <?php endif;?>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'text_with_wide_image' ):?>
                               <div class="flex-section wide-image-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <?php $position_of_text_and_image = get_sub_field('position_of_text_and_image');?>
                                   <?php if($position_of_text_and_image == 'img_left' ){?>
                                       <div class="row">
                                           <div class="large-6 medium-6 medium-offset-1 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <?php if($image = get_sub_field('image')){?>
                                                           <?php
                                                           $image_size = 'wide-content-image';
                                                           $image_size_2 = 'wide-content-image-2';
                                                           $image_alt = $image['alt'];
                                                           $image_thumb = $image['sizes'][ $image_size ];
                                                           $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                           $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                           $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                           ?>
                                                           <div class="image_image image_wrapper">
                                                               <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                    srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                             <?php echo $image_thumb; ?> 1x"
                                                                    alt="<?php echo $image_left_alt;?>">
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="large-3 medium-3 medium-offset-1 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <div class="section-content">
                                                           <?php $text = get_sub_field('text');?>
                                                           <?php if($text){?>
                                                               <div class="text">
                                                                   <?php echo $text;?>
                                                                   <?php
                                                                   $button_title = get_sub_field('section_button_title');
                                                                   $button_type_of_link = get_sub_field('section_button_type');
                                                                   $button_external_link = get_sub_field('section_button_external_link');
                                                                   $button_page_link = get_sub_field('section_button_page_link');
                                                                   $button_file_link = get_sub_field('section_button_file_link');
                                                                   ?>
                                                                   <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                       <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } ?>
                                                                   <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   <?php } elseif ($position_of_text_and_image == 'img_right'){ ?>
                                       <div class="row">
                                           <div class="large-6 medium-6 medium-push-5 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <?php if($image = get_sub_field('image')){?>
                                                           <?php
                                                           $image_size = 'wide-content-image';
                                                           $image_size_2 = 'wide-content-image-2';
                                                           $image_alt = $image['alt'];
                                                           $image_thumb = $image['sizes'][ $image_size ];
                                                           $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                           $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                           $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                           ?>
                                                           <div class="image_image image_wrapper">
                                                               <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                    srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                             <?php echo $image_thumb; ?> 1x"
                                                                    alt="<?php echo $image_left_alt;?>">
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="large-3 medium-3 medium-pull-5 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <div class="section-content">
                                                           <?php $text = get_sub_field('text');?>
                                                           <?php if($text){?>
                                                               <div class="text">
                                                                   <?php echo $text;?>
                                                                   <?php
                                                                   $button_title = get_sub_field('section_button_title');
                                                                   $button_type_of_link = get_sub_field('section_button_type');
                                                                   $button_external_link = get_sub_field('section_button_external_link');
                                                                   $button_page_link = get_sub_field('section_button_page_link');
                                                                   $button_file_link = get_sub_field('section_button_file_link');
                                                                   ?>
                                                                   <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                       <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } ?>
                                                                   <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   <?php }?>
                               </div>
                           <?php elseif( get_row_layout() == 'text_with_medium_image' ):?>
                               <div class="flex-section medium-image-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <?php $position_of_text_and_image = get_sub_field('position_of_text_and_image');?>
                                   <?php if($position_of_text_and_image == 'img_left' ){?>
                                       <div class="row">
                                           <div class="large-5 medium-5 medium-offset-1 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <?php if($image = get_sub_field('image')){?>
                                                           <?php
                                                           $image_size = 'medium-content-image';
                                                           $image_size_2 = 'medium-content-image-2';
                                                           $image_alt = $image['alt'];
                                                           $image_thumb = $image['sizes'][ $image_size ];
                                                           $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                           $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                           $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                           ?>
                                                           <div class="image_image image_wrapper">
                                                               <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                    srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                             <?php echo $image_thumb; ?> 1x"
                                                                    alt="<?php echo $image_left_alt;?>">
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="large-4 medium-4 medium-offset-1 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <div class="section-content">
                                                           <?php $text = get_sub_field('text');?>
                                                           <?php if($text){?>
                                                               <div class="text">
                                                                   <?php echo $text;?>
                                                                   <?php
                                                                   $button_title = get_sub_field('section_button_title');
                                                                   $button_type_of_link = get_sub_field('section_button_type');
                                                                   $button_external_link = get_sub_field('section_button_external_link');
                                                                   $button_page_link = get_sub_field('section_button_page_link');
                                                                   $button_file_link = get_sub_field('section_button_file_link');
                                                                   ?>
                                                                   <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                       <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } ?>
                                                                   <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   <?php } elseif ($position_of_text_and_image == 'img_right'){ ?>
                                       <div class="row">
                                           <div class="large-5 medium-5 medium-push-7 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <?php if($image = get_sub_field('image')){?>
                                                           <?php
                                                           $image_size = 'medium-content-image';
                                                           $image_size_2 = 'medium-content-image-2';
                                                           $image_alt = $image['alt'];
                                                           $image_thumb = $image['sizes'][ $image_size ];
                                                           $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                           $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                           $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                           ?>
                                                           <div class="image_image image_wrapper">
                                                               <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                    srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                 <?php echo $image_thumb; ?> 1x"
                                                                    alt="<?php echo $image_left_alt;?>">
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="large-5 medium-5 medium-pull-4 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <div class="section-content">
                                                           <?php $text = get_sub_field('text');?>
                                                           <?php if($text){?>
                                                               <div class="text">
                                                                   <?php echo $text;?>
                                                                   <?php
                                                                   $button_title = get_sub_field('section_button_title');
                                                                   $button_type_of_link = get_sub_field('section_button_type');
                                                                   $button_external_link = get_sub_field('section_button_external_link');
                                                                   $button_page_link = get_sub_field('section_button_page_link');
                                                                   $button_file_link = get_sub_field('section_button_file_link');
                                                                   ?>
                                                                   <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                       <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } ?>
                                                                   <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   <?php }?>
                               </div>
                           <?php elseif( get_row_layout() == 'text_with_small_image' ):?>
                               <div class="flex-section small-image-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <?php $position_of_text_and_image = get_sub_field('position_of_text_and_image');?>
                                   <?php if($position_of_text_and_image == 'img_left' ){?>
                                       <div class="row">
                                           <div class="large-2 medium-2 medium-offset-1 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <?php if($image = get_sub_field('image')){?>
                                                           <?php
                                                           $image_size = 'small-content-image';
                                                           $image_size_2 = 'small-content-image-2';
                                                           $image_alt = $image['alt'];
                                                           $image_thumb = $image['sizes'][ $image_size ];
                                                           $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                           $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                           $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                           ?>
                                                           <div class="image_image image_wrapper">
                                                               <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                    srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                 <?php echo $image_thumb; ?> 1x"
                                                                    alt="<?php echo $image_left_alt;?>">
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="large-8 medium-8 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <div class="section-content">
                                                           <?php $text = get_sub_field('text');?>
                                                           <?php if($text){?>
                                                               <div class="text">
                                                                   <?php echo $text;?>
                                                                   <?php
                                                                   $button_title = get_sub_field('section_button_title');
                                                                   $button_type_of_link = get_sub_field('section_button_type');
                                                                   $button_external_link = get_sub_field('section_button_external_link');
                                                                   $button_page_link = get_sub_field('section_button_page_link');
                                                                   $button_file_link = get_sub_field('section_button_file_link');
                                                                   ?>
                                                                   <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                       <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } ?>
                                                                   <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   <?php } elseif ($position_of_text_and_image == 'img_right'){ ?>
                                       <div class="row">
                                           <div class="large-2 medium-2 medium-push-9 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <?php if($image = get_sub_field('image')){?>
                                                           <?php
                                                           $image_size = 'small-content-image';
                                                           $image_size_2 = 'small-content-image-2';
                                                           $image_alt = $image['alt'];
                                                           $image_thumb = $image['sizes'][ $image_size ];
                                                           $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                           $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                           $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                           ?>
                                                           <div class="image_image image_wrapper">
                                                               <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                    srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                 <?php echo $image_thumb; ?> 1x"
                                                                    alt="<?php echo $image_left_alt;?>">
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>

                                           </div>
                                           <div class="large-8 medium-8 medium-pull-1 small-12 columns">
                                               <div class="section-table">
                                                   <div class="section-cell">
                                                       <div class="section-content">
                                                           <?php $text = get_sub_field('text');?>
                                                           <?php if($text){?>
                                                               <div class="text">
                                                                   <?php echo $text;?>
                                                                   <?php
                                                                   $button_title = get_sub_field('section_button_title');
                                                                   $button_type_of_link = get_sub_field('section_button_type');
                                                                   $button_external_link = get_sub_field('section_button_external_link');
                                                                   $button_page_link = get_sub_field('section_button_page_link');
                                                                   $button_file_link = get_sub_field('section_button_file_link');
                                                                   ?>
                                                                   <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                       <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                       <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                                   <?php } ?>
                                                                   <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   <?php }?>
                               </div>
                           <?php elseif( get_row_layout() == 'table_with_image' ):?>
                               <div class="flex-section table-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="section-content">
                                       <div class="row">
                                           <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                                               <?php $text = get_sub_field('text');?>
                                               <?php if($text){?>
                                                   <span class="text"><?php echo $text;?></span>
                                               <?php } ?>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="large-8 medium-8 medium-offset-1 small-12 columns">
                                               <?php if( have_rows('table') ):?>
                                                   <table>
                                                       <tbody>
                                                       <?php while ( have_rows('table') ) : the_row();?>
                                                           <tr>
                                                               <?php if( have_rows('table_row') ):?>
                                                                   <?php while ( have_rows('table_row') ) : the_row();?>
                                                                       <td>
                                                                           <?php $table_column = get_sub_field('table_column');?>
                                                                           <?php if($table_column){?>
                                                                               <span class="table_column"><?php echo $table_column;?></span>
                                                                           <?php } ?>
                                                                       </td>
                                                                   <?php endwhile;?>
                                                               <?php endif;?>
                                                           </tr>
                                                       <?php endwhile;?>
                                                       </tbody>
                                                   </table>
                                               <?php endif;?>
                                           </div>
                                           <div class="large-2 medium-2 small-12 columns">
                                               <?php if($image = get_sub_field('image')){?>
                                                   <?php
                                                   $image_size = 'small-content-image';
                                                   $image_size_2 = 'small-content-image-2';
                                                   $image_alt = $image['alt'];
                                                   $image_thumb = $image['sizes'][ $image_size ];
                                                   $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                   $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                   $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                   ?>
                                                   <div class="image_image image_wrapper">
                                                       <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                            srcset="<?php echo $image_thumb_2; ?> 2x,
                                                             <?php echo $image_thumb; ?> 1x"
                                                            alt="<?php echo $image_left_alt;?>">
                                                   </div>
                                               <?php } ?>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'image_section' ):?>
                               <?php $counter++;?>
                               <?php
                               $image_section = get_sub_field('image');
                               $size_section = 'image-section';
                               $thumb_section = $image_section['sizes'][ $size_section ];
                               ?>
                               <style>
                                   <?php if($image_section){?>
                                   #image-id-<?php echo $counter;?>.image-section {
                                       background-image:url(<?php echo $thumb_section;?>);
                                   }
                                   <?php }?>
                               </style>
                               <div class="image-section" id="image-id-<?php echo $counter;?>"></div>
                               <?php unset($image_section, $size_section, $thumb_section);?>
                           <?php elseif( get_row_layout() == 'video_section' ):?>
                               <div class="flex-section video-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="section-content">
                                       <div class="row">
                                           <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                                               <?php $text = get_sub_field('text');?>
                                               <?php if($text){?>
                                                   <span class="text"><?php echo $text;?></span>
                                               <?php } ?>
                                               <?php $iframe_for_video = get_sub_field('iframe_for_video');?>
                                               <?php if($iframe_for_video){?>
                                                   <div class="iframe_for_video"><?php echo $iframe_for_video;?></div>
                                               <?php } ?>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'opsomming' ):?>
                               <div class="flex-section opsomming-section <?php if(get_sub_field('use_lists_with_styles')){ echo 'special-li';}?> <?php echo get_sub_field('section_background_color');?>">
                                   <div class="row">
                                       <div class="large-4 medium-4 medium-offset-1 small-12 columns">
                                           <div class="section-table">
                                               <div class="section-cell">
                                                   <div class="section-content">
                                                       <?php $text = get_sub_field('text');?>
                                                       <?php if($text){?>
                                                           <div class="text">
                                                               <?php echo $text;?>
                                                               <?php
                                                               $button_title = get_sub_field('section_button_title');
                                                               $button_type_of_link = get_sub_field('section_button_type');
                                                               $button_external_link = get_sub_field('section_button_external_link');
                                                               $button_page_link = get_sub_field('section_button_page_link');
                                                               $button_file_link = get_sub_field('section_button_file_link');
                                                               ?>
                                                               <?php if($button_type_of_link == 'external' && $button_title){?>
                                                                   <a target="_blank" class="btn" href="<?php echo $button_external_link;?>"><?php echo $button_title;?></a>
                                                               <?php } elseif ($button_type_of_link == 'page' && $button_title) {?>
                                                                   <a class="btn" href="<?php echo $button_page_link;?>"><?php echo $button_title;?></a>
                                                               <?php } elseif ($button_type_of_link == 'file' && $button_title) { ?>
                                                                   <a target="_blank" class="btn" href="<?php echo $button_file_link;?>"><?php echo $button_title;?></a>
                                                               <?php } ?>
                                                               <?php unset($button_title,$button_type_of_link,$button_external_link,$button_page_link,$button_file_link);?>
                                                           </div>
                                                       <?php } ?>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="large-5 medium-5 medium-offset-1 small-12 columns">
                                           <div class="section-table">
                                               <div class="section-cell">
                                                   <?php if($image = get_sub_field('image')){?>
                                                       <?php
                                                       $image_size = 'medium-content-image';
                                                       $image_size_2 = 'medium-content-image-2';
                                                       $image_alt = $image['alt'];
                                                       $image_thumb = $image['sizes'][ $image_size ];
                                                       $image_thumb_2 = $image['sizes'][ $image_size_2 ];
                                                       $width = ($image['sizes'][ $image_size_2 . '-width' ])/2;
                                                       $height = ($image['sizes'][ $image_size_2 . '-height' ])/2;
                                                       ?>
                                                       <div class="image_image image_wrapper">
                                                           <img width="<?php echo $width;?>" height="<?php echo $height;?>" class="image_element" src="<?php echo $image_thumb; ?>"
                                                                srcset="<?php echo $image_thumb_2; ?> 2x,
                                                                             <?php echo $image_thumb; ?> 1x"
                                                                alt="<?php echo $image_left_alt;?>">
                                                       </div>
                                                   <?php } ?>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           <?php elseif( get_row_layout() == 'accordion' ):?>
                               <?php if( have_rows('faq_sections') ):?>
                                   <div class="row">
                                       <div class="large-10 medium-10 medium-offset-1 small-12 columns">
                                           <div class="faq_sections_wrapper accordion">
                                               <?php while ( have_rows('faq_sections') ) : the_row();?>
                                                   <?php $section_title = get_sub_field('section_title');?>
                                                   <?php if($section_title){?><span class="section_title accordion-toggle"><?php if($date_in_title = get_sub_field('date_in_title')){?><span class="date_in_title"><?php echo $date_in_title;?></span><?php } ?><?php echo $section_title;?></span>
                                                   <?php } ?>
                                                   <?php $section_content = get_sub_field('section_content');?>
                                                   <?php if($section_content){?>
                                                       <div class="section_content accordion-content clearfix"><?php echo $section_content;?></div>
                                                   <?php } ?>
                                               <?php endwhile;?>
                                           </div>
                                       </div>
                                   </div>
                               <?php endif;?>
                           <?php elseif( get_row_layout() == 'separator' ):?>
                               <div class="separator"></div>
                           <?php endif;?>
                       <?php endwhile;?>
                   </div>
               <?php endif;?>
           <?php endwhile;?>
       <?php endif; ?>
   </main>

<?php get_footer(); ?>