<?php

function tmd_includes() {
    // CSS
    wp_enqueue_style("style.css", get_stylesheet_uri());

    // JavaScript
    wp_enqueue_script("jquery");
    wp_enqueue_script("main.min.js", get_template_directory_uri() . "/js/main.min.js", array(), "1.0.0", true);
}
add_action("wp_enqueue_scripts", "tmd_includes");

?>