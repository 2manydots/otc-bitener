<footer class="footer" id="footer">
    <div class="footer-info">
        <div class="outer-wrapper">
            <?php if(get_field('footer_tagline', 'option')){?>
                <div class="row">
                    <div class="large-12 columns">
                        <h2><?php the_field('footer_tagline', 'option');?></h2>
                    </div>
                </div>
            <?php }?>
            <div class="row">
                <div class="large-4 medium-4 columns">
                    <?php $domainName = $_SERVER['SERVER_NAME'];?>
                    <?php if( $domainName == 'ie-bitener-com.otcbitener.staging.wpengine.com' || $domainName == 'www.bitener.ie') { ?>
                        <div class="footer-logo">
                            <a target="_blank" href="http://www.theotclab.com/">
                                <img class="rowa-logo" src="<?php echo get_template_directory_uri(); ?>/images/sprites/rowa-logo.svg" >
                            </a>
                        </div>
                    <?php }else{?>
                        <div class="footer-logo"><a target="_blank" href="http://www.theotclab.com/"><?php new Sprite('logo-otc'); ?></a></div>
                    <?php } ?>
                </div>
                <div class="large-7 medium-8 columns">
                    <div class="footer-contant">
                        <div class="row">
                            <div class="medium-4 columns">
                                <div class="footer-sections"><?php the_field('footer_section_1', 'option');?></div>
                            </div>
                            <div class="medium-4 columns">
                                <div class="footer-sections"><?php the_field('footer_section_2', 'option');?></div>
                            </div>
                            <div class="medium-4 columns">
                                <div class="footer-sections"><?php the_field('footer_section_3', 'option');?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="copy">The OTC Lab &copy; <?= date('Y') ?> &middot; <a href="<?php the_field('legal_policy_page', 'option');?>" target="_blank" class="link">Legal policy</a><?php $domainName = $_SERVER['SERVER_NAME'];?><?php if(($domainName == 'www.menorelax.nl') || ($domainName == 'menorelax.nl')) { ?>- KOAG KAG 4339-0716-1093</p><?php } ?>
</footer>
<?php wp_footer(); ?>
</body>
</html>