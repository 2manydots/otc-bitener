<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since Grunt Boilerplate 0.1.0
 * @author 2manydots
 */

get_header(); ?>

<main class="main">
  <div class="row">
    <div class="large-12 columns">

    <?php

        get_template_part('parts/breadcrumbs');

        get_template_part('parts/no-content');

    ?>
    </div>
  </div><!-- row -->

</main>

<?php get_footer(); ?>
